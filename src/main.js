import Vue from 'vue'
import App from './App.vue'

export const eventBus = new Vue({
	data: {
		quotes: ['Just a Quote to start with something!']
	},
	methods: {
		addQuote(quote) {
			this.quotes.push(quote);
		},
		removeQuote(id) {
			this.quotes = this.quotes.splice(id, 1);
		}
	}
});

new Vue({
	el: '#app',
	render: h => h(App)
})
